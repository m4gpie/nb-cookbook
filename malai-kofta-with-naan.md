# Malai Kofta with Naan

The Indian veggie replacement to meatballs is generally made with paneer, but it can be turned into
vegan by replacing the paneer with tofu. The koftas are generally fried and thrown into a dense
spicy tomato curry sauce, although we served them apart because the koftas didn't have much
integrity. I also made some naan breads to eat the kofta, and some basmati
rice, but one naan each and 5 koftas was good enough to be properly stuffed.

`#naan` `#indian` `#vegan`


## Ingredients

**Perfect for 5 people: 6 large naans and 25 koftas**

For the naan:

* White flour (2 cups)
* Cane sugar (2 teaspoons)
* Dry yeast (1 teaspoon)
* Salt (1/2 teaspoon)
* Warm water (3/4 cups)
* Soy yogurt (4 tablespoons)
* Olive oil (2 teaspoons)

For the koftas:

* Potatoes (6 small)
* Tofu (450 gr)
* **Corn starch (1/2 cup)**
* Lemon juice (1 lemon)
* Minced cilantro (loads)
* Salt
* Garam masala (or whatever mix of spices can be sorted out)

For the tomato curry:

* Onion (2 medium)
* Chopped tomatoes (2 tins)
* Garlic (4 cloves, thinly chopped)
* Ginger (Nice piece, grated)
* Cashews (1 cup)
* Garam masala (or whatever mix of spices, etc)
* Turmeric powder
* Chili flakes
* Salt
* Soy cream or Coconut milk (1 can)
* Soy yogurt (for garnish on top)

## Method 

1. Start with the koftas. Boil the potatoes whole and with the skin (to stop the water from getting
   in). When they are soft, mash them and add mashed tofu. It should be a really nice paste, not
   chunky. Add the lemon, the chopped cilantro, the spices, and try until reaching the wanted
   saltines.

2. They can be deep-fried or placed in an oiled oven tray and baked for ~40 minutes. 

3. For the curry, saute the onions really well until they are soft, add the garlic, the ginger, all
   the spices and the cashews. Saute it all together with a high flame. Then add the tomato tins,
   the soy cream or coconut milk and leave it in a low flame to simmer for some time. After 20
   minutes, blend it all together until it's really smooth, add some more cashews and leave to
   simmer in the pot at a low flame till the koftas are ready.

4. Throw in the koftas. You can also not do this, and directly serve the koftas dry and pour some
   curry on top. All is good.

5. To do the naan, start preparing the dough with an hour or two. Mix the dry ingredients (flour,
   cane sugar, yeast and salt), on a separate bowl mix the wet stuff (water, yogurt and oil) and
   then throw it all together. Mix it to form a dough. It will be incredibly sticky and tacky.
   Knead it and try to accomplish a ball, although this might be complicated. Cover it up and
   leave it to rest for an hour or two.

6. Sprinkle a flat surface with flour, through the dough in and knead it till you are satisfied. Cut
   it into six pieces and you are ready to cook them.

7. Each piece, flatten it out with a rolling pin until it's big enough (they can easily cover a 28cm
   diameter pan). Heat the pan or skillet, grease it with some oil, and throw the beautiful naan in.
   Once it's bubbling and it's cooked on one side, with a brush spread some oil on the top side and
   flip it. Cook the other side.

8. Make a mix of butter, minced garlic and parsley. When the naan is done, spread the butter over it
   and leave it in a hot place. You can create an oily heap of naans with butter. Everyone will be
   happy

## Changelog

#### First attempt: _2021-02-12_

* The naans were incredible. 
* We messed it up with the koftas a little bit. We tried to fry them on a pan with not much oil, and
they were completely ruined. Then we put them in the oven without covering the tray with oil and
they got stuck. Nonetheless, they were delicious.
* I didn't have garam masala, so I added cardamom, cumin seeds, cilantro seeds, some other stuff
    that I found in the cupboard, grated it all together and roasted it on some oil. It tasted like
    spices, so I guess that's a mission accomplished.
