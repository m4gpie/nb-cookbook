# Tadka Dal 

First time cooking a delicious dal that could resemble the flavours of the every-day yellow dal.
Even without having curry leafs (which adds so much flavor), ghee or green chillies, the result was
extremely tasty. Had it with some jira rice and a couple of the always successful flatbreads.

`#dal` `#indian` `#vegan`


## Ingredients

**For 4 bowls of dal**

Dal:
* Yellow lentils / dried chana dal (200 grams or 1 cup)
* Water (1 liter or 4 cups)
* One big onion, chopped
* Green chillies (or some spicy padrón peppers in my case)
* 6 garlic cloves, finely chopped
* Ginger (small piece, around 1 spoon, grated)
* 2 tomatos, chopped
* Cumin seeds (1 spoon)
* Turmeric powder (1 tablespoon)
* Garam masala (1/2 tablespoon)
* Salt
* Olive or sunflower oil

Tadka:
* Olive or sunflower oil
* A couple of dry tomatoes
* Dried chillies (or cayenne peppers, or chilly flakes)
* Mustard seeds
* Cumin seeds
* Coriander seeds
* Fenugreek seeds
* Garam masala

## Method 

1. Soak lentils, rinse and leave to soak for some time (30-60 minutes)

2. Fry the chillies in hot oil until. Chop the onions and start browning them along with the
   chilies. Chop the garlic and the ginger and add it to the onions. Chop the tomatoes and add them
   to the mix when the onions have softened. Add the cumin seeds and leave to cook until it's all a
   delicious brown soft paste.

3. Add lentils, water, turmeric and salt. Bring to simmer and leave cooking at low heat for 1 hour
   with the lid on.

4. Remove the lid so that the water start to evaporate and the dal thickens. Add the garam masala,
   adjust the salt, and stir.

5. Make the tadka: in a small pan, heat some oil, add the seeds and roast them. Then add the
   chillies and the spices being careful so that they don't burn. When the tadka is ready, add to
   the dal and stir all together.

6. Eat with some rice and bread


## Changelog

#### First attempt: _2021-06-14_

* Used padrón pepper instead of green chillies, and sundried tomato instead of red dry chilly.
* It was delicious.
