# Empanada de Atún 

This is my mom's recipe for her amazing empanada, which has always been the protagonist of birthdays
and other celebrations since I was a child. The traditional filling is a mix of tuna and tomato
sauce, and the pasty is covered with a layer of battered egg to make it crunchy and glossy, which
makes this recipe non-vegan, although really easy to adapt for a vegan home.

`#empanada` `#spanish`


## Ingredients

**For 6/8 rations**

For the dough:

* Flour (400gr)
* Water (warm, 150ml)
* Olive oil (150ml)
* Salt (1 tablespoon)
* Baking powder (1 teaspoon)

For the filling:

* Onions (2 medium ones)
* Peppers (1 red, 1 green)
* Tin of tomato sauce
* 2 medium tins of tuna

Finishing:
* 1 egg


## Method 

1. Start making the dough. Mix the dry ingredients in a bowl (flour, salt and baking powder). Mix
   in the wet ingredients (water and oil) and knead for a while until the dough is soft. It will be
   really oily and quite wet, but it should be possible to knead comfortably and form a ball. Passed
   minimum of 10 minutes kneading it, leave it to rest while you make the filling.

2. Cut the onions and the peppers in thin slices and saute them with a pinch of salt until they are
   really soft and nearly caramelised. Add the tomato sauce and the tuna, and mix all together.

3. With a bit of flour over the counter, throw the dough ball and fold/knead a couple of times. Make
   an even ball and cut to parts in a 3/5 - 2/5 proportion. Keep the smallest one aside and with a
   rolling pin flatten and roll the biggest one. Once it's thin enough (1-2mm) and it can cover
   completely the tray, place it with some extra border to make the folds.

4. Preheat the oven at 170ºC. Put in the fill, flatten the other ball of dough and place it on top.
   Roll together the border so that it keeps closed, if there is too much border, cut it out and use
   it to make some crunchy drawing on top.

5. For the crunchy drawing: roll the dough excess into a thin tube and place it over the finished
   empanada, pressing lightly.

6. Beat an egg and with a brush, spread lightly over the top. With a fork, puncture some holes all
   over the surface of the empanada so that the air can come out and it does not inflate. Put it in
   the oven for 30-40minutes (until you see that the dough is done, golden and crunchy

![Empanada](./assets/empanada.jpg)


## Changelog

#### First attempt: _2021-03-23_

* I have been doing this recipe all my life, but I've never had a clear idea of the flour-water-oil
    proportion. This is THE ONE, the dough is tasty, crunchy, easy to handle, and extremely
    delicious.
