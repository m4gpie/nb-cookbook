# Sourdough pizza

Best pizza dough we have ever made (and probably tried) was our first attempt to make sourdough
pizza for 5. We made 4 pizzas (two vegetarian, two vegan) which came out amazing, but we invested
quite a lot of time, starting the dough on the day before. Anyway, if there's time to indulge to
some slow cooking, this pizza is amazing and it's totally worth a try

We made some mistakes while handling the dough and forming the bases, as we didn't have enough trays
to form them and leave them to rest quietly. This meant that the second batch didn't come out so
well, but we took some learnings out of this.

Nonetheless, the pizzas were all amazing, even the vegan ones (lol), and everyone was extremely
satisfied. Mostly the cooks ;)


`#sourdogh` `#vegan` `#vegetarian` `#pizza`

![Sourdough pizza deliciousness](./assets/sourdough-pizza-5.jpg)


## Ingredients

**Makes 4 large pizzas**

For the pizza dough:
* 300gr of sourdough leaven (risen overnight)
* 600gr of flour
  * 15% wholemeal or other
  * 85% strong white bread flour
* 410ml of water
* 10gr salt
* 10ml olive oil

For the toppings:
* Tomato sauce:
  * Two tins of tomato
  * Loads of herbs (mostly oregano)
  * Cayenne pepper
  * Sugar, salt, olive oil
* Two onions
* Two different-colored peppers
* Mushroom sausages
* Mushrooms
* Black and green olives
* Blue cheese
* Goat cheese
* Vegan melting cheese
* Vegan blue cheese

## Method 

1. Prepare the sourdough leaven and leave it overnight to rise and create all the delicious yeast.
   Parting from an initial ~70 gr of sourdough starter, we added 120gr of white flour and 120ml of
   water, stirred it and left it on a warm place for a night. The result was around 300gr of leaven.

2. Create the pizza dough in the morning. We used the breadmaker to knead the dough, which took
   around two hours. Once this was done, drop the dough into a large bowl and using some flour, fold
   it few times and try to form a ball to leave it to rest. The dough is **extremely** wet and
   sticky, so don't worry about it. Leave it in a warm place to rest another 5 hours.

3. Around two hours before serving the pizza, drop the dough into a surface with some flour and
   knead it for a while. If the dough has had enough time, it will have formed gluten and it won't
   be as sticky as before, although it should be really flexible and soft. Knead it for a while and
   form a ball. Cut the ball in four equal pieces (weighting around 350-400gr each) and form a
   little ball with each one of them. Leave them to rest after forming some tension and spreading
   some flour on the surface.

![Sourdough pizza deliciousness](./assets/sourdough-pizza-1.jpg)


4. After an hour of rest, make the pizza base. The dough is enough flexible so you can use its own
   weight to slowly spread it into a circle, hanging from your hands. Once the base is done, set it
   on a floured tray and leave it to rest for a bit. If you don't have enough trays to rest the
   pizza base, it's better to leave them in ball form, and flatten them immediately before cooking
   them.

5. Preheat the oven at 220ºC.

6. Add the tomato sauce to the pizza base, and cook it for 7 minutes.

7. Take the pizza out, add the rest of the toppings and cook it for other 7 minutes. This depends on
   what toppings you add, if they are many and humid, it might need some more time to cook
   completely. We cooked them for 10 minutes on this second round.

8. Done. Eat.
  

## Changelog

#### First attempt: _2021-01-08_

* Made 4 pizzas, two vegetarian and two vegan:
  * Toppings 1: delicious tomato sauce, caramelized onion and pepper, vegan sausages, green olives and
mozzarella cheese (or vegan cheese)
  * Toppings 2: delicious tomato sauce, laminated mushrooms, blue cheese, goat cheese and mozzarella
(or vegan cheeses)
* We spread out the second batch bases on a wooden surface because we didn't have enough trays,
ultimately they broke and we had to re-knead and re-flatten them again, with worse results than the
first batch. Better to leave them to rest like a ball and flatten them immediately before topping
and cooking them.

