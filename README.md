# Cookbook

This notebook repo contains the culinary experiments of a couple of computer geeks. That's why they
are in markdown, on git, and each recipe has a changelog.

We sincerely apologise.

## Index 

* [Sourdough pizza](./sourdough-pizza.md)
* [Chinese dumplings](./chinese-dumplings.md)
* [Flatbreads with mole](./flatbreads-with-mole.md)
* [Malai kofta with naan](./malai-kofta-with-naan.md)
